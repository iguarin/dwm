# Ian's Build of dwm (suckless' dynamic window manager)

## Patches and Features

- My build of [slstatus](https://gitlab.com/iguarin/slstatus/)
- Beautiful gruvbox rice
- [cursorwarp](https://dwm.suckless.org/patches/cursorwarp/) allows you to focus cursor while switching between windows via (`super+K/J`)
- [actualfullscreen](https://dwm.suckless.org/patches/actualfullscreen/) fixes a bug with games and fullscreen applications where switching tags would completely black the application out
- [alwayscenter](https://dwm.suckless.org/patches/alwayscenter/) fixes a stupid function in dwm which spawns floating windows (such as those present in steam) in the top-right corner (`0,0`) as opposed to the center.
- [fullgaps](https://dwm.suckless.org/patches/fullgaps/) adds sexy gaps around windows, which sizes are adjustable with (`super+-/+`)
- [attachabove](https://dwm.suckless.org/patches/attachabove/) overrides the default tiling functionality in dwm where new windows become the new master. New windows are now placed above the currently selected window.
- [fixborders](https://dwm.suckless.org/patches/alpha/) fixes an issue where running dwm with picom makes window borders translucent, restoring them back to their default (solid) state
- [noborder](https://dwm.suckless.org/patches/noborder/) removes dwm's border on tags with only one window as well as monocle mode
- [selective fakefullscreen](https://dwm.suckless.org/patches/selectivefakefullscreen/) allows you to specify which windows to make fake fullscreen (make fullscreening say, a video, take up only its window size and not actually fullscreen) via `config.def.h`
- [systray](https://dwm.suckless.org/patches/systray/) adds a systray to dwm
- [status2d](https://dwm.suckless.org/patches/status2d) allows the colourization of slstatus my build of dwm specifically, but also has features like rectangle drawing in dwm's bar.
- [statusallmons](https://dwm.suckless.org/patches/statusallmons/) applies dwm's statusbar (whether it be slstatus, dwmblocks, etc.) to all monitors on a multi-monitor setup.
## Pre-requisites

- Roboto (system font) and fontawesome (unicode glyphs) (otherwise edit `config.def.h` to use other fonts)
- Qutebrowser (webrowser) (otherwise edit `config.def.h` to use another browser)
- Kitty (terminal) (otherwise edit `config.def.h` to use another terminal)
- Pamixer (pulseaudio/pipewire volume control) (otherwise delete the keyboard shortcuts in `config.def.h`)
- Corectrl (custom rule I have to spawn corectrl on the last tag) (otherwise delete the window rule in `config.def.h`)

## Installation

```
git clone https://github.com/iguarin/dwm
cd dwm
sudo make clean install
```
